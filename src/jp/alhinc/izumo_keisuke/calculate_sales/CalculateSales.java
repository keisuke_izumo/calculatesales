package jp.alhinc.izumo_keisuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		/* 		売上集計システム		*/
		/* 		3月15日					*/
		/* 		出雲　圭佑				*/

		BufferedReader br = null;
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();
		List<String> nameList = new ArrayList<>();

		//支店定義ファイルを読み込む
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		File file = new File(args[0], "branch.lst");
		//支店定義ファイルが存在していない場合エラー
		if(!file.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}
		try {
			FileReader fr = new	FileReader(file);
			br = new BufferedReader(fr);
			String line;
			//支店コードと対応する支店名を保持する
			while((line = br.readLine()) != null) {
				String[] linesp = line.split(",");
				if(linesp.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				String branchCode = linesp[0];
				//支店コードが三桁の数字以外エラー
				if(!branchCode.matches("[0-9]{3}") ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				String branchName = linesp[1];
				nameMap.put(branchCode, branchName);
				//先に売上マップのキーに支店マップのキーを入れておく
				salesMap.put(branchCode, (long)0);
			}
			/*コマンドライン引数から売上ファイルを検索する
			 *検索条件（拡張子がrcdかつ数字8桁）*/
			File salesFile = new File(args[0]);
			File[] salesList  = salesFile.listFiles();
			int count = 0;
			long sales = 0;
			for(int i = 0; i < salesList.length; i++) {
				//ファイル名を取得
				String salesName = salesList[i].getName();
				//売上ファイル検索条件（拡張子がrcdかつ数字８桁のファイル）
				if(salesName.matches("[0-9]{8}.*rcd$") && salesList[i].isFile()) {
					nameList.add(salesName);
				}
			}
			//売上ファイル名連番チェック
			for(int i = 0; i < nameList.size()-1 ; i++) {
				//ファイル名から拡張子を除外
				String nextsalesrcd = nameList.get(i+1).substring(0,8);
				int number = Integer.parseInt(nextsalesrcd);
				String salesrcd = nameList.get(i).substring(0,8);
				int number2 = Integer.parseInt(salesrcd);
				if(number - number2 != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
	        }
			for(int i = 0; i < nameList.size() ; i++) {
				File salesfile = new File(args[0], nameList.get(i));
				FileReader salesfr = new FileReader(salesfile);
				br = new BufferedReader(salesfr);
				String salesline;
				List<String> proceedsList = new ArrayList<>();
				//支店コード、売上額を抽出
				while((salesline = br.readLine()) != null) {
					count++;
					proceedsList.add(salesline);
				}
				//売上ファイルの行数が2行以外のときにエラー
				if(count != 2) {
					System.out.println(nameList.get(i) + "のフォーマットが不正です");
					return;
				}
				count = 0;
				//売上ファイルの支店コードが支店定義ファイルに存在しないとき
				if(!nameMap.containsKey(proceedsList.get(0))) {
					System.out.println(salesList[i].getName() + "の支店コードが不正です");
					return;
				}
				//売上金額に数字以外が含まれているとき
				if(!proceedsList.get(1).matches("[0-9]+")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				sales = Long.parseLong(proceedsList.get(1));
				sales += salesMap.get(proceedsList.get(0));
				int digits = String.valueOf(sales).length();
				if(digits > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				salesMap.put(proceedsList.get(0), sales);
				proceedsList.remove(1);
				proceedsList.remove(0);
			}
		} catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//支店別集計ファイル出力メソッド呼び出し
		boolean fo = fileOut(args[0], "branch.out", salesMap, nameMap);
		if(!fo) {
			return;
		}
	}

	//支店別集計ファイル出力メソッド
	public static boolean fileOut(String commandLine, String fileName, HashMap<String, Long> salesMap, HashMap<String, String> nameMap ) {
		//支店別集計ファイルを出力する
		BufferedWriter bw = null;
		boolean authenticity = true;
		try {
			File newfile = new File(commandLine , fileName);
			FileWriter fw = new FileWriter(newfile);
			bw = new BufferedWriter(fw);
			for (String key : salesMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			authenticity = false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					authenticity = false;
				}
			}
		}
		return authenticity;
	}
}
